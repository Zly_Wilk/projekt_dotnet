﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.ML;
using Projekt_dotnet2ML.Model;

namespace projekt_dotnet2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //path to file with dataset
        //in this example file from: https://www.kaggle.com/karimali/used-cars-data-pakistan
        private const string DATA_FILEPATH = @"C:\Users\Wilk\Desktop\Nowy folder\OLX_Car_Data_CSV.csv";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Oblicz(object sender, RoutedEventArgs e)
        {
            try
            {

                ModelInput modelInput = new ModelInput();

                modelInput.Brand = textBox0.Text;
                modelInput.Condition = textBox1.Text;
                modelInput.Fuel = textBox2.Text;
                if (textBox3.Text.Length == 0) modelInput.KMs_Driven = 0;
                else modelInput.KMs_Driven = float.Parse(textBox3.Text);
                modelInput.Model = textBox4.Text;
                modelInput.Registered_City = "";
                modelInput.Transaction_Type = textBox7.Text;
                if (textBox3.Text.Length == 0) modelInput.Year = 0;
                else modelInput.Year = float.Parse(textBox6.Text);
                modelInput.Price = 0;

                ModelOutput predictionResult = ConsumeModel.Predict(modelInput);

                textOutput.Text = "Przewidywana cana auta: " + predictionResult.Score + "PKR";
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
                MessageBox.Show("Błąd");
            }

        }

    }
}
