// This file was auto-generated by ML.NET Model Builder. 

using Microsoft.ML.Data;

namespace Projekt_dotnet2ML.Model
{
    public class ModelInput
    {
        [ColumnName("Brand"), LoadColumn(0)]
        public string Brand { get; set; }


        [ColumnName("Condition"), LoadColumn(1)]
        public string Condition { get; set; }


        [ColumnName("Fuel"), LoadColumn(2)]
        public string Fuel { get; set; }


        [ColumnName("KMs Driven"), LoadColumn(3)]
        public float KMs_Driven { get; set; }


        [ColumnName("Model"), LoadColumn(4)]
        public string Model { get; set; }


        [ColumnName("Price"), LoadColumn(5)]
        public float Price { get; set; }


        [ColumnName("Registered City"), LoadColumn(6)]
        public string Registered_City { get; set; }


        [ColumnName("Transaction Type"), LoadColumn(7)]
        public string Transaction_Type { get; set; }


        [ColumnName("Year"), LoadColumn(8)]
        public float Year { get; set; }


    }
}
